var map;
var table;
var markers = [];
var api_endpoint = 'XBUS_API_ENDPOINT';
var drawingManager;
var all_overlays = [];
var filterString = "";

function initialize() {
    var mapOptions = {
        zoom: 5,
        center: new google.maps.LatLng(35.8574708,104.1361118),
        //disableDefaultUI: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
            position: google.maps.ControlPosition.TOP_RIGHT
        },
        zoomControlOptions: {
            position: google.maps.ControlPosition.RIGHT_CENTER
        },
        panControlOptions: {
            position: google.maps.ControlPosition.RIGHT_CENTER
        }
    }
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

    drawingManager = new google.maps.drawing.DrawingManager({
        drawingControl: true,
        drawingControlOptions: {
          position: google.maps.ControlPosition.RIGHT_TOP,
          drawingModes: [
            google.maps.drawing.OverlayType.RECTANGLE
          ]
        }
      });
      drawingManager.setMap(map);

      google.maps.event.addListener(drawingManager, 'overlaycomplete', function(e) {
          for (var i=0; i < all_overlays.length; i++)
              all_overlays[i].overlay.setMap(null);
          all_overlays = [];
          all_overlays.push(e);
	  ne = e.overlay.getBounds().getNorthEast();
	  sw = e.overlay.getBounds().getSouthWest();
	  filterString = 'geo:[' + sw.lat() + ',' + sw.lng() + ' TO ' + ne.lat() + ',' + ne.lng() + ']';
          search_tag($('.list-box a.btn.btn-info'));
      });
}

var deselect_btn = function(obj) {
    if (is_btn_clicked(obj))
        $(obj).click();
}

var set_btn_tag_name = function(obj, text) {
    $(obj).find('span.tag-name').text(text);
    $(obj).find('span.tag-name').attr('data-tag-name', text);
}

var get_btn_tag_name = function(obj) {
    return $(obj).find('span.tag-name').attr('data-tag-name');
}

//close search result box
var clear_search_result = function() {
    var btn = '.current-search-tag a.btn';
    deselect_btn(btn);

    $('.search-term').text('');
    $(btn + ' .badge').text('0');

    $('.current-search-tag').addClass('hide');
};

var is_tag_exist = function(search_from, tag) {
    return $(search_from).find('[data-tag-name="'+tag+'"]').length > 0;
}

var is_btn_clicked = function(obj) {
    return $(obj).hasClass('btn-info');
}

//click on tag
$('html').on('click', '.list-box a.btn', function() {
    $('.list-box a.btn').removeClass('btn-info').addClass('btn-default');
    $(this).toggleClass('btn-default').toggleClass('btn-info');
    search_tag(this);
    if (markers != undefined) {
        for (var x in markers) {
            markers[x].setMap(null);
        }
        markers.length = 0;
    }
    return false;
});

$('html').on('click', '.current-search-tag .close', function() {
    $(".search-query").val('');
    clear_search_result();
    return false;
});

//save tag
$('html').on('click', '.list-box .save-tag-btn', function() {
    var term = get_btn_tag_name($(this).parent());
    packed_json = {};
    packed_json['tag_name'] = term;
    packed_json['poi_counts'] = table.api().data().length;
    packed_json['poi'] = {};
    $.each(table.api().data(), function(k, v) {
        packed_json['poi'][k] = v;
        packed_json['poi'][k]['icon_type'] = 0;
    });
    $.ajax({url: api_endpoint + "/api_save_search_result", 
        data: JSON.stringify(packed_json), 
        dataType: "json", processData: false, type: "post", 
        statusCode: {
            200: function(data) {
                get_tags();
            },
            500: function() {
                alert("BOOM");
            }
        }
    });
    return false;
});

//delete tag
$('html').on('click', '.list-box .tag .close', function() {
    if (! confirm('移除此標籤？'))
        return false;

    deselect_btn($(this).parent());

    var term = get_btn_tag_name($(this).parent());
    $.ajax({url: api_endpoint + "/api_delete_tag", data: "tag_name="+term, dataType: "json", type: "get", 
        statusCode: {
            200: function(data) {
                $('.list-tag span.tag-name[data-tag-name="'+term+'"').parents('.btn').remove();
            },
            500: function() {
                alert("BOOM");
            }
        }
    });
    return false;
});

$('html').on('click', '.find-on-map', function() {
    if (markers != undefined) {
        for (var x in markers) {
            markers[x].setMap(null);
        }
        markers.length = 0;
    }

    var latlng = $(this).attr('title');
    var cht = $(this).data('cht');
    var infowindow = new google.maps.InfoWindow({
        content: '<div class="info-window"><strong>' + cht + '</strong><br>' + latlng + '</div>'
    });
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(parseFloat(latlng.split(',')[0]), parseFloat(latlng.split(',')[1])),
        map: map, title: cht, latlng: latlng
    });
    google.maps.event.addListener(marker, 'click', function() {
        for (var n in markers) {
            markers[n].infowindow.close();
        }
        infowindow.open(map, marker);
    });
    marker.infowindow = infowindow;
    markers.push(marker);
    google.maps.event.trigger(marker, 'click');
    map.panTo(marker.getPosition());
    return false;
});

//get_tags
var get_tags = function() {
    $.ajax({url: api_endpoint + "/api_get_tags", dataType: "json", type: "get",
        statusCode: {
            200: function(data) {
                $.each(data, function(k, v) {
                    if (! is_tag_exist('.list-tag', v.tag_name)) {
                        tag = '<a class="tag btn btn-default"><span class="close">×</span>'
                              + '<span class="tag-name" data-tag-name="'+v.tag_name+'">'+v.tag_name+'</span> <span class="badge">'+v.poi_counts
                              +'</span></a>'+"\n";
                        $('.list-tag').append(tag);
                    }
                });
            },
            500: function() {
                alert("BOOM");
            }
        }
    });
}

//submit search form
$('html').on('submit', '#search-form', function() {
    clear_search_result();

    var btn = '.current-search-tag a.btn';
    deselect_btn(btn);
    set_btn_tag_name(btn, $(".search-query").val());
    $(btn).click();

    $('.current-search-tag').removeClass('hide');
    return false;
});

//search tag
var search_tag = function(obj) {
    var term = get_btn_tag_name(obj);
    $('.search-term').text(' ('+term+')');

    table.api().ajax.url( api_endpoint + "/api_search_tag?search_tag=" + term + "&fq=" + filterString).load();
}

$(function() {
    get_tags();
    initialize();
    table = $('#table').dataTable({
        /*"columnDefs": [{
            "targets": [0],
            "bSortable": false
        }],*/
        "order": [[ 2, "asc" ]],
        "paging": false,
        "searching": false,
        "info": false,
        "ajax": {
            "url": "test.json",
            "dataSrc": "poi"
        },
        "columns": [
            { "data": "cht", "className": "" },
            { "data": "geo" },
            //{ "data": "icon_type" },
            { "data": "_version_" }
        ],
        "fnCreatedRow": function( nRow, aData, iDataIndex ) {
            var text = "";
            aData.latlng = aData.geo.pop();
            if (aData.latlng != "x,y") {
                text = "<a class=\"btn btn-link find-on-map\" data-cht=\""+aData.cht+"\" title=\""+aData.latlng+"\">定位</a>"
            }
            $('td:eq(2)', nRow).html(text);
        }
    });

    table.api().on('xhr', function ( e, settings, json ) {
        if (json.tag_name == get_btn_tag_name('.current-search-tag a.btn'))
            $('.current-search-tag').find('span.badge').text(json.poi_counts);

        $('.sidebar-inside').perfectScrollbar('destroy');
        $('.sidebar-inside').perfectScrollbar({suppressScrollX: true});
    });

    $('.sidebar-inside').perfectScrollbar({suppressScrollX: true});
});
